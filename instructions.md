# DRAFT DRAFT DRAFT DRAFT DRAFT DRAFT 

This is a **DRAFT** of a document for editing, and none of its
content should be take literally at this point.

# The Great LaunchPad Bug Conversion

Win swag by converting bugs from LaunchPad's bug tracker to GitLab's
bug tracker.

NOTE: no coding is needed, you're not fixing bugs, you're just migrating
them from LaunchPad to GitHub, or, for stale bugs, closing them on LaunchPad.

## Instructions

  - pick an open bug [on LaunchPad](https://bugs.launchpad.net/).
    A bug is open if (a) it's not tagged `bug-migration`, and (b) its status
    is one of the following: 'New', 'Incomplete', 'Opinion', 'Confirmed',
    'Triaged', 'In Progress'.
  - edit the bug's tags and add the `bug-migration` tag - doing this immediately
    minimizes the chances someone else will start working on the same bug
  - decide whether the bug needs to be migrated to GitLab, of it it
    can be closed without further work on LaunchPad (because it's
    no longer valid etc.)

### Migration

  - in the new bug [on GitLab](https://gitlab.com/groups/inkscape/-/issues)
    include, in the bug description, the following back link:
    ```markdown
    [LP:XXX](https://bugs.launchpad.net/inkscape/+bug/XXX)
    ```
    where XXX is the LaunchPad bug number, e.g. 166554.  
  - in the closing comment on LaunchPad, include the following link:
    ```markdown
    [moved to GitLab](https://gitlab.com/inkscape/extensions/issues/42)
    ```
    replacing the URL with the URL of the new issue on GitLab. 

  - To migrate a bug, you need to do the following:
    - read over the comments associated with the bug, and write a summary
      comment
    - check attachments associated with the bug make sense
    - FIXME: what else?

  - It is not necessary to copy, verbatim, all the comments and attachments
    from the Launchpad bug to the GitLab bug, as we can still reference
    the Launchpad bug for details.  But please do copy / paste key comments
    and examples into your summary comment.

### Closing on LaunchPad

For stale bugs that can simply be closed on LaunchPad, with no new bug
on GitLab:

  - In the closing comment on LaunchPad, include the text:
    ```markdown
    Closed by: https://gitlab.com/username
    ```
    replacing `username` with your GitLab username.  That way you get credit for
    closing the bug even though it isn't listed on GitLab.

The goal of this game is to have human minds, as opposed to an automated script,
migrate or close all the Launchpad bugs so Inkscape can move forward using
GitLab its bug-tracker.  So we're not expecting people to *fix* bugs, although if
you see a quick fix and just do it that's great.  But in general, you would close
a bug without migrating it because the bug is no longer relevant (can't reproduce,
doesn't apply anymore, incomplete with evidence of a failed attempt to get more
details, etc.).

## Rewards

Each LaunchPad bug converted to a GitLab bug or closed on LaunchPad will score
points based on the bug's value, calculated [as shown below](#bug-scores).  You
can check your progress on [this leader board](FIXME).  Scores are calculated
by a program which scans the bug lists about once an hour.  
The biggest reward, of course ;-), is knowing you've helped the Inkscape
project move forward, but some “bugs” (issues on Launchpad) have an associated
“Bug” (collectable badge / sticker).

Finding the “Bugs” has multiple benefits.  They add significant additional
points to the [score for the associated “bug”](bug-scores) (issue).  You get to
collect the set of badges on the [scoreboard](FIXME).  Best of all, thanks to
CR, at the end of the game (when all the “bugs” are migrated), you'll be mailed
a sticker for each type of “Bug” you collected.  There will also be a special
prize for the top three highest scores.

## Bug scores

“bug”: one of about 5000 bugs reported on Launchpad
“Bug”: a collectable as described in [Rewards](rewards).

The goal of this game is to migrate *all* of the open bugs from Launchpad to
GitLab, so in some sense no bug is more valuable than another.  But some bugs
will take more work to migrate than others, so points are assigned as follows:

 - 5 points - base line score for all bugs
 - 2 points - per person commenting on the bug, not counting the original reporter
 - 1 point - per comment on the bug
 - 1 point - per hundred non-blank lines of comments on the bug
 - 3 points - per attachment on the bug
 - XX points - for a Bug associated with the bug - these are the Bugs you can
   collect as described in [Rewards](#rewards).  Sometimes Bugs associated with
   bugs will be listed, but sometimes you'll only find out you scored these extra
   points, and collected this Bug, when you've migrated the bug from Launchpad to GitLab.

   
