"""
launchpad_sync.py - collect bug data from Launchpad

TerryNBrown@gmail.com Sat Nov 10 11:58:27 CST 2018
"""
import os
import time
from collections import namedtuple
from datetime import datetime
from pprint import pprint
import re
import sys

from launchpadlib.launchpad import Launchpad

import keyvalstore
from keyvalstore import get_val, put_val, keys_starting

ClosedBug = namedtuple("ClosedBug", "lp_bug_id closer")

closer_re = re.compile(r"Closed by: https://gitlab.com/(?!\d)(\w{2,20})")
moved_re = re.compile(
    r"\[moved to GitLab]\(https://gitlab.com/inkscape/(\w+)/issues/(\d+)\)"
)

CACHEDIR = "/home/tbrown/t/Proj/bugstats/lp_cache"
DB_FILENAME = "bugstat.db.sqlite"


def dt_to_ts(dt):
    return time.mktime(dt.timetuple())


def fetch_lp_bug(lp_bug_id, __connection=[]):
    """Fetch bug data from Launchpad

    __connection *is not an argument*, it's a cached DB connection,
    usage is: `fetch_lp_bug(lp_bug_id)`
    """
    if not __connection:
        __connection[:] = [
            Launchpad.login_anonymously(
                'just testing', 'production', CACHEDIR, version='devel'
            )
        ]
    launchpad = __connection[0]

    bug = launchpad.bugs[lp_bug_id]
    ans = {k: getattr(bug, k) for k in bug.lp_attributes}
    for k, v in list(ans.items()):
        if isinstance(v, datetime):
            ans[k] = dt_to_ts(v)
    ans['attachment_count'] = len(bug.attachments)
    ans['messages'] = []
    for message in bug.messages:
        ans['messages'].append(
            {
                'content': message.content,
                'date_created': dt_to_ts(message.date_created),
                'author': {
                    'username': message.owner.name,
                    'name': message.owner.display_name,
                },
            }
        )

    return ans


def get_lp_bug(lp_bug_id, max_age=None):
    """Get a dict representing a Launchpad bug.

    If our (DB) cache entry is older than max_age seconds, check
    remote for updates.

    Args:
        lp_bug_id (int): Launchpad bug ID
        max_age (float): max. age of our cache entry, default: no age limit

    Returns: dict representing Launchpad bug.
    """
    key = ('lp_bug', lp_bug_id)
    bug = get_val(key)

    now = time.time()
    if bug == {} or (
        max_age is not None and now - bug['last_fetch'] > max_age
    ):
        print("Fetching Launchpad issue #%s" % lp_bug_id)
        bug['bug'] = fetch_lp_bug(lp_bug_id)
        bug['last_fetch'] = now
        put_val(key, bug)
    return bug


def update_lp_issues(max_age=3600, last_n_days=10):
    """update Launchpad issues updated since the last time we updated

    Updates `lp_last_update`.

    Args:
        max_age (int): do nothing if we last updated less than max_age
            seconds ago
        last_n_days (int): if not previous update, go back this many days

    Or updated in the `last_n_days`, if we have no prev. update.
    """

    prev = get_val('lp_last_update') or 0
    now = time.time()
    if now - prev < max_age:
        print("Skipping Launchpad update")
        return
    if not prev:
        prev = time.time() - 86400 * last_n_days
    lp = Launchpad.login_anonymously(
        'just testing', 'production', CACHEDIR, version='devel'
    )
    since = datetime.fromtimestamp(prev)
    print("Querying Launchpad for updated issues")
    lp_bug_ids = set(get_val('lp_bug_ids'))
    inkscape = lp.projects['inkscape']
    tasks = inkscape.searchTasks(modified_since=since)
    for task in tasks:
        # update local cache copy of issue
        get_lp_bug(task.bug.id, max_age=0)
        lp_bug_ids.add(task.bug.id)
    put_val('lp_last_update', now)
    put_val('lp_bug_ids', list(lp_bug_ids))

    if 0:
        launchpad = Launchpad.login_anonymously(
            'just testing', 'production', CACHEDIR, version='devel'
        )
        inkscape = launchpad.projects['inkscape']
        print(dir(inkscape))
        print(inkscape.lp_attributes)
        print(inkscape.lp_collections)
        print(inkscape.lp_entries)
        tasks = inkscape.searchTasks()
        for task in tasks[:10]:
            print(task.title)
        bug = tasks[0].bug
        print(bug.description)  # == bug.messages[0].content
        print(bug.messages[1].content)


def fake_scoring_lp_issues():
    gl_bug_ids = get_val('gl_bug_ids')
    lp_bug_ids = get_val('lp_bug_ids')
    ans = []
    minlen = min(len(gl_bug_ids), len(lp_bug_ids))
    for i in range(minlen - 5, min(minlen + 10, len(lp_bug_ids))):
        ans.append((lp_bug_ids[i], 'bryceharrington'))
    return ans


def get_closer(text):
    closer = closer_re.search(text)
    return closer.group(1) if closer else None


def get_moved_to(text):
    moved = moved_re.search(text)
    return (moved.group(1), moved.group(2)) if moved else None


def scoring_lp_issues():
    ans = []
    for lp_bug_id in get_val('lp_bug_ids'):
        msgs = get_lp_bug(lp_bug_id)['messages']
        if msgs:
            closer = get_closer(msgs[-1])
            if closer:
                ans.append(ClosedBug(lp_bug_id=lp_bug_id, closer=closer))
    return ans


def main():
    keyvalstore.DB_FILENAME = DB_FILENAME
    # pprint(get_lp_bug(1772883, max_age=None))
    update_lp_issues(last_n_days=10)
    ### put_val('lp_last_update', time.time())
    # lp_bug_ids = set(get_val('lp_bug_ids'))
    # for key in keys_starting('lp_bug'):
    #     key = eval(key)
    #     lp_bug_ids.add(key[1])
    #     print(repr(key))
    # put_val('lp_bug_ids', list(i for i in lp_bug_ids if isinstance(i, int)))
    if len(sys.argv) > 1:
        get_lp_bug(int(sys.argv[1]), max_age=0)


if __name__ == "__main__":
    main()

