import json
import os
import time
from collections import defaultdict, namedtuple
from pprint import pprint

import jinja2

from keyvalstore import get_val, put_val, keys_starting
import keyvalstore

from gitlab_sync import DB_FILENAME
from gitlab_sync import get_gl_bug, fake_scoring_gl_issues
from launchpad_sync import get_lp_bug, fake_scoring_lp_issues
from launchpad_sync import get_lp_bug

Level = namedtuple("Level", "name score")
Special = namedtuple("Special", "pts comment bio")
# these are filled in by load_data()
LEVELS = []
SPECIALS = {}
SPECIAL = {}

ScoreNote = namedtuple("ScoreNote", "comment pts lp_bug_id")
SpecialScoreNote = namedtuple(
    "SpecialScoreNote", "special comment pts lp_bug_id"
)


def load_data():
    LEVELS[:] = [Level(**i) for i in json.load(open('levels.json'))]
    SPECIALS.update(json.load(open('specials.json')))
    SPECIAL.update(
        {k: Special(**v) for k, v in json.load(open('special.json')).items()}
    )


def score_for(lp_bug_id):
    notes = []

    notes.append(
        ScoreNote(lp_bug_id=lp_bug_id, pts=5, comment="Basic bug score")
    )
    notes.append(
        ScoreNote(lp_bug_id=lp_bug_id, pts=3, comment="N users in discussion")
    )
    notes.append(
        ScoreNote(lp_bug_id=lp_bug_id, pts=1, comment="N lines of discussion")
    )

    for special in SPECIALS.get(str(lp_bug_id), []):
        spec = SPECIAL[special]
        notes.append(
            SpecialScoreNote(
                special=special,
                comment=spec.comment,
                pts=spec.pts,
                lp_bug_id=lp_bug_id,
            )
        )

    return sum(i.pts for i in notes), notes


def main():
    keyvalstore.DB_FILENAME = DB_FILENAME
    load_data()

    log = []
    errors = False
    authors = defaultdict(lambda: {'gl': {}, 'lp': {}})

    # X gl_proj = get_val('gl_project')  # for project names for URLs

    # bugs claimed by [LP:xxx](https://bugs.launchpad.net/inkscape/+bug/xxx)
    # notes on GL
    for gl_bug_id, lp_bug_id, username in fake_scoring_gl_issues():
        authors[username]['gl'][gl_bug_id] = lp_bug_id
        gl_bug = get_gl_bug(gl_bug_id)
        # X pprint(gl_bug['bug'])
        log.append(
            "INF: %s claims LP:%s in GL: %s"
            % (username, lp_bug_id, gl_bug['bug']['web_url'])
        )

    # bugs claimed by Closed by: https://gitlab.com/username
    # notes in LP
    for lp_bug_id, username in fake_scoring_lp_issues():
        authors[username]['lp'][lp_bug_id] = lp_bug_id
        log.append("INF: %s claims LP:%s" % (username, lp_bug_id))

    # check for multiple claims
    claimants = defaultdict(lambda: set())
    multiple = set()
    for author in authors:
        for lp_bug_id in authors[author]['gl'].values():
            claimants[lp_bug_id].add(author)
        for lp_bug_id in authors[author]['lp'].values():
            claimants[lp_bug_id].add(author)
    for lp_bug_id in claimants:
        if len(claimants[lp_bug_id]) != 1:
            log.append(
                "ERR: claims for %s: %s" % (lp_bug_id, claimants[lp_bug_id])
            )
            multiple.add(lp_bug_id)
            errors = True

    # calc. individual scores
    scores = []
    specials = {}
    for author in authors:
        specials[author] = []
        auth_log = []
        total = 0
        # this silently ignores multiple claims for the same bug,
        # e.g. using both migration and closing annotations mistake
        scoring = set(
            list(authors[author]['gl'].values())
            + list(authors[author]['lp'].values())
        )
        for lp_bug_id in scoring & multiple:
            auth_log.append(
                "ERR: no score for %s, multiple claimants" % lp_bug_id
            )
        scoring -= multiple
        for lp_bug_id in scoring:
            pts, notes = score_for(int(lp_bug_id))
            specials[author].extend(notes)
            total += pts
            auth_log.append("INF: %s for %s" % (pts, lp_bug_id))
            for note in notes:
                if isinstance(note, SpecialScoreNote):
                    auth_log.append(
                        "     %s for %s Bug" % (note.pts, note.special)
                    )
                if isinstance(note, ScoreNote):
                    auth_log.append(
                        "     %s for %s" % (note.pts, note.comment)
                    )
        scores.append((author, total))
        auth_log.append("INF: %s total" % total)
        with open("log/%s.html" % author, 'w') as logfile:
            logfile.write("<pre>\nINF: run at %s\n" % time.asctime())
            logfile.write('\n'.join(auth_log))
            logfile.write("</pre>\n")

    scores.sort(key=lambda x: x[1], reverse=True)

    authors = []
    for author, score in scores:
        stats = {}
        authors.append(stats)
        lvl = [score >= i.score for i in LEVELS]
        lvl_i = lvl.index(True)
        lvl = LEVELS[lvl_i]
        stats['level'] = lvl.name
        stats['score'] = score
        stats['username'] = author
        if lvl.name != LEVELS[0].name:
            stats['percent'] = int(
                (score - LEVELS[lvl_i].score)
                / (LEVELS[lvl_i - 1].score - lvl.score)
                * 100
            )
            stats['next_lvl'] = LEVELS[lvl_i - 1].score - score
        stats['specials'] = []
        multi = {}
        for note in specials[author]:
            if isinstance(note, SpecialScoreNote):
                if note.special in multi:
                    multi[note.special]['count'] += 1
                    if 'href' in multi[note.special]:
                        del multi[note.special]['href']
                else:
                    info = note._asdict()
                    multi[note.special] = info
                    info['count'] = 1
                    info['href'] = (
                        "https://bugs.launchpad.net/inkscape/+bug/%s"
                        % note.lp_bug_id
                    )
                    stats['specials'].append(info)

    src_dir = os.path.join(os.path.dirname(__file__), 'templates')
    env = jinja2.Environment(loader=jinja2.FileSystemLoader(src_dir))
    links = [
        {'url': "index.html", 'text': "Scoreboard"},
        {'url': "catalog.html", 'text': "Bug list"},
        {'url': "known.html", 'text': "Known bugs"},
    ]
    base_context = dict(links=links, updated=time.asctime())

    template = env.get_template('scoreboard.html')
    context = dict(base_context, page={'url': "index.html"}, users=authors)
    with open("index.html", 'wb') as out:
        out.write(template.render(context).encode('utf-8'))

    template = env.get_template('catalog.html')
    context = dict(base_context, page={'url': "catalog.html"},
        bugs=[dict(v._asdict(), name=k) for k, v in SPECIAL.items()],
    )
    with open("catalog.html", 'wb') as out:
        out.write(template.render(context).encode('utf-8'))

    template = env.get_template('known.html')
    context = dict(base_context, page={'url': "known.html"},
        bugs=SPECIALS, special=SPECIAL
    )
    with open("known.html", 'wb') as out:
        out.write(template.render(context).encode('utf-8'))

    with open("log/bugstats_log.html", 'w') as logfile:
        logfile.write("<pre>\nINF: run at %s\n" % time.asctime())
        logfile.write('\n'.join(log))
        logfile.write("</pre>\n")


if __name__ == "__main__":
    main()
