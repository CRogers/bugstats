"""
FIXME: add JSON dump / import / update so good data can be saved
and imported again.
"""
import json
import os
import sqlite3


def _concur():
    """Get connection and cursor for DB"""
    if not os.path.isfile(DB_FILENAME):
        con = sqlite3.connect(DB_FILENAME)
        con.execute("create table keyval(key text, val text)")
        con.execute("create unique index keyval_key_idx on keyval (key)")
    con = sqlite3.connect(DB_FILENAME)
    return con, con.cursor()


def _key_to_text(key):
    """Convert tuple key to text key"""
    return str(key)


def get_val(key, __concur=[]):
    """Get val for key from key/val store, must be JSON compatible

    __concur *is not an argument*, it's a cached DB connection,
    usage is: `get_val(key)`.
    """
    if not __concur:
        __concur[:] = con, cur = _concur()
    con, cur = __concur
    cur.execute("select val from keyval where key = ?", [_key_to_text(key)])
    res = cur.fetchall()
    return json.loads(res[0][0]) if res else {}


def put_val(key, val, __concur=[]):
    """Put val for key in key/val store, must be JSON compatible

    __concur *is not an argument*, it's a cached DB connection,
    usage is: `put_val(key, val)`.
    """
    if not __concur:
        __concur[:] = _concur()
    con, cur = __concur
    cur.execute(
        "insert or replace into keyval values (?, ?)",
        [_key_to_text(key), json.dumps(val, sort_keys=True)],
    )
    con.commit()


def drop_val(key, __concur=[]):
    if not __concur:
        __concur[:] = _concur()
    con, cur = __concur
    cur.execute("delete from keyval where key = ?", [_key_to_text(key)])
    con.commit()


def keys_starting(key, __concur=[]):
    """Return a list of keys starting with key

    eg. for ('gl_bug',) return [('gl_bug', 123), ('gl_bug', 6434), ...]
    """
    if not __concur:
        __concur[:] = _concur()
    con, cur = __concur

    if not isinstance(key, (list, tuple)):
        key = [key]
    key = "(%s, %%" % ', '.join(repr(i) for i in key)
    cur.execute("select key from keyval where key like ?", [key])
    return list(i[0] for i in cur)
